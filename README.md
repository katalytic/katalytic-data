# katalytic-data [![version](https://img.shields.io/pypi/v/katalytic-data)](https://pypi.org/project/katalytic-data/) [![tests](https://gitlab.com/katalytic/katalytic-data/badges/main/pipeline.svg?key_text=tests&key_width=38)](https://gitlab.com/katalytic/katalytic-data/-/commits/main) [![coverage](https://gitlab.com/katalytic/katalytic-data/badges/main/coverage.svg)](https://gitlab.com/katalytic/katalytic-data/-/commits/main) [![docs](https://img.shields.io/readthedocs/katalytic-data.svg)](https://katalytic-data.readthedocs.io/en/latest/) [![license: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

**Don't use in production yet.**
I will probably introduce backwards incompatible changes

TODO: features and link to docs

## Example (TODO)

## Installation
By itself
```bash
pip install katalytic-data
```

As part of the [katalytic](https://gitlab.com/katalytic/katalytic) collection
```bash
pip install katalytic
```

## Contributing
We appreciate any form of contribution, including but not limited to:
- **Code contributions**: Enhance our repository with your code and tests.
- **Feature suggestions**: Your ideas can shape the future development of our package.
- **Architectural improvements**: Help us optimize our system's design and API.
- **Bug fixes**: Improve user experience by reporting or resolving issues.
- **Documentation**: Help us maintain clear and up-to-date instructions for users.

